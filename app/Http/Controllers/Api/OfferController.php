<?php

namespace App\Http\Controllers\Api;

use App\Data\Models\Products;
use App\Data\Models\Voucher;
use App\Data\Repositories\CategoryRepository;
use App\Data\Repositories\GalleryRepository;
use App\Data\Repositories\OfferRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class OfferController extends Controller
{
    protected $_repository;
    const PER_PAGE = 10;

    public function __construct(OfferRepository $repository)
    {
        $this->_repository = $repository;
    }

    public function index(Request $request)
    {
        $requestData = $request->all();
        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $per_page = self::PER_PAGE;
        $data = $this->_repository->findByAll($pagination,$per_page,$requestData);
        $output = [
            'data' => $data['data'],
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Categories Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    // Check Voucher
    public function applyCoupon(Request $request)
    {
        $requestData = $request->all();

        $data = Voucher::where('name', $requestData['coupon_code'])->first();

        if(!$data) {

            $output = ['error'=>['code' => 406,'message' => "please enter a valid coupon code"]];
            return response()->json($output, 406);

        } else {

            $expiry_date = new Carbon($data->expiry_date);

            if(Carbon::now()->greaterThan($expiry_date)) {
                $output = ['error'=>['code' => 406,'message' => "your voucher is expired"]];
                return response()->json($output, 406);
            }

            if($data->status == 0) {
                $output = ['error'=>['code' => 406,'message' => "your voucher is disabled"]];
                return response()->json($output, 406);
            }

            if($requestData['total_amount'] < 20) {
                $output = ['error'=>['code' => 406,'message' => "your order must be atleast £20 to get this voucher"]];
                return response()->json($output, 406);
            }

            $discountedAmount = round($requestData['total_amount']*$data['discount']/100,  2);


            $output = ['data' => $discountedAmount, 'message' => __("messages.success")];
            return response()->json($output, Response::HTTP_OK);
        }
    }
}
