<?php

namespace App\Http\Controllers\Api;

use App\Data\Models\Orders;
use App\Data\Models\Products;
use App\Data\Models\UserDevices;
use App\Data\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use App\PayPal;
use App\User;
use Braintree\ClientToken;
use Braintree\Gateway;
use Cartalyst\Stripe\Exception\CardErrorException;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;
use Validator;
use Cartalyst\Stripe\Stripe;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    protected $_repository;
    const PER_PAGE = 10;

    public function __construct(OrderRepository $repository)
    {
        $this->_repository = $repository;
    }

    public function getAllOrders(Request $request)
    {
        $requestData = $request->all();
        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $per_page = self::PER_PAGE;

        $data = $this->_repository->findByAll($pagination, $per_page, $requestData);

        $output = [
            'data' => $data['data'],
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Orders Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function updateOrderStatus(Request $request, $id)
    {
        $requestData = $request->all();
        $requestData['id'] = $id;

        $validator = Validator::make($requestData, [
            'id' => 'required|exists:orders,id',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->updateRecord($requestData, $id);

        $output = ['data' => $data, 'message' => "your order status has been updated successfully"];
        return response()->json($output, Response::HTTP_OK);
    }

    public function stripePayment(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'user_id' => 'required',
            'total_amount_with_fee' => 'required',
            'delivery_fees' => 'required',
            'payment' => 'required',
            'delivery_address' => 'required',
            'order_details' => 'required|array',
            'order_details.*.price' => 'required|numeric',
            'order_details.*.product_id' => 'required|numeric',
            'order_details.*.product_name' => 'required',
            'order_details.*.quantity' => 'required'
        ]);

        $user = User::firstOrCreate(
            [
                "email" => $requestData['user_data']['email']
            ],
            [
                "password" => Hash::make(Str::random(12)),
                "name" =>  $requestData['user_data']['name'],
                "phone_number" => $requestData['user_data']['number']
            ]
        );

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        try {

            $payment = $user->charge(
                $requestData['total_amount_with_fee']*100,
                $requestData['payment_method_id']
            );

            $payment = $payment->asStripePaymentIntent();

            $data = $this->_repository->placeOrder($requestData);

        } catch (IncompletePayment $e) {

            if ($e instanceof PaymentActionRequired) {

                $code = 401;
                $output = ['error' => ['code' => 402, 'payment_data' => $e->payment]];
                return response()->json($output, $code);

            } else {

                $code = 401;
                $output = ['error' => ['code' => 401, 'message' => $e->getMessage()]];
                return response()->json($output, $code);
            }

        }

        if ($data) {

            $requestData['user_id'] = $data['user_id'];
            $requestData['phone_number'] = $data['phone_number'];
            $requestData['total_amount'] = $requestData['total_amount_with_fee'];

            $this->sendNotification($data);
        }

        $output = ['data' => $requestData, 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    /** Send Push Notification */
    public function sendNotification($data)
    {
        $devices = UserDevices::get()->pluck('device_token')->toArray();

        $push = new PushNotification('fcm');

        $push->setMessage([
            'data' => [
                'title' => 'This is the title',
                'body' => 'New order has been placed to your restaurant',
                'sound' => 'default',
                'order_id' => $data->id,
                'total_amount'=> $data->total_amount_with_fee,
                'reference'=>$data->reference,
                'order_type'=> $data->order_type,
                'payment'=>$data->payment,
                'delivery_time' => $data->delivery_time
            ]
        ])->setDevicesToken($devices)->send();

        $response = $push->getFeedback();
    }

    public function paypalPayment($requestData)
    {
        $paypal = new PayPal;

        $card = new CreditCard(array(
            'firstName'             => $requestData['user_data']['name'],
            'lastName'              =>  $requestData['user_data']['name'],
            'number'                => $requestData['user_data']['card_no'],
            'expiryMonth'           => $requestData['user_data']['expiration_month'],
            'expiryYear'            => $requestData['user_data']['expiration_year'],
            'cvv'                   => $requestData['user_data']['cvc'],
           /* 'billingAddress1'       => '1 Scrubby Creek Road',
            'billingCountry'        => 'AU',
            'billingCity'           => 'Scrubby Creek',
            'billingPostcode'       => '4999',
            'billingState'          => 'QLD',*/
        ));

        $response = $paypal->purchase([
            'amount' => $paypal->formatAmount($requestData['total_amount_with_fee']),
            'currency' => 'USD',
            'card'     => $card,
            'description'   => 'This is a test purchase transaction.',
        ]);

        return $response;
    }

    public function getTotalSales(Request $request)
    {
        $requestData = $request->all();

        $validator =  Validator::make($requestData, [
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->getTotalSales($requestData);

        $output = ['data' => ['total_sales' => $data, 'most_sale_item' => "Margherita Pizza", 'tota_orders' => 15], 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    public function placeOrder(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'user_id' => 'required',
            'total_amount_with_fee' => 'required',
            'delivery_fees' => 'required',
            'payment' => 'required',
            'delivery_address' => 'required',
            'order_details' => 'required|array',
            'order_details.*.price' => 'required|numeric',
            'order_details.*.product_id' => 'required|numeric',
            'order_details.*.product_name' => 'required',
            'order_details.*.quantity' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->placeOrder($requestData);

        if ($data) {

            $requestData['user_id'] = $data['user_id'];
            $requestData['phone_number'] = $data['phone_number'];
            $requestData['total_amount'] = $requestData['total_amount_with_fee'];

            $this->sendNotification($data);
        }

        $output = ['data' => $requestData, 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    public function generateToken() {

        $gateway = new Gateway([
            'environment' => env('BTREE_ENVIRONMENT'),
            'merchantId' => env('BTREE_MERCHANT_ID'),
            'publicKey' => env('BTREE_PUBLIC_KEY'),
            'privateKey' => env('BTREE_PRIVATE_KEY')
        ]);

        $clientToken = $gateway->clientToken()->generate();

        $output = [
            'data' => $clientToken,
            'message' => "Generated Token",
        ];
        return response()->json($output, Response::HTTP_OK);

    }



    public function generatePayPalToken()
    {
        $url = "https://api-m.sandbox.paypal.com/";

        $clientID =  "AQIpYBZ5N3y_48pHp9LSOmhPeUawTonWOXEJtKSu4IEmT23qenOs46wsyRl1MAGANW56jqfqHrW0GJ_m";
        $secretKey = "ENGK9dfsa-5ZbDJnfyN3zDOcelrEr6UEfy7nAHXzq0JApWryp29ZBZSw9GRu7kgTc_K2v3j7hpi7d4BT";

        $headers = array(
            'Content-Type:application/json',
          //  'Authorization: Basic '. base64_encode("user:password") // <---
            'Authorization: Basic '. base64_encode($clientID.":".$secretKey)
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url.'v1/oauth2/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic '.base64_encode($clientID.":".$secretKey),
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response);



        $data['access_token'] = $result->access_token;


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $url.'v1/identity/generate-token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$data['access_token']
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response);

        $data['client_token'] = $result->client_token;

        return response()->json($data, Response::HTTP_OK);
    }

    public function payPalOrder(Request $request){
        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'amount' => 'required',
            'access_token' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }


        $url = "https://api-m.sandbox.paypal.com/";



        $req = array();
        $req['intent'] = "CAPTURE";
        $req['purchase_units'] =

        $object = new \stdClass();
        $object->amount = array('currency_code' => "GBP",'value'=>$requestData['amount']);
        $myArray[] = $object;

        $req['purchase_units'] = $myArray;

       $payload =  json_encode($req);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url.'v2/checkout/orders',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$payload,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer '.$requestData['access_token']
            ),
        ));

        $response = curl_exec($curl);
        $output = json_decode($response);

        return response()->json($output, Response::HTTP_OK);


    }
}
